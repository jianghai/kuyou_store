module.exports = function(grunt) {
    'use strict';
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            vjs: {
                src: '../vjs/vjs.min.js',
                dest: 'lib/vjs.min.js'
            },
            trunk: {
                src: '../trunk/trunk.min.js',
                dest: 'lib/trunk.min.js'
            },
            kuyou_css: {
                src: '../kuyou/kuyou.css',
                dest: 'kuyou.css'
            },
            'jquery.extend': {
                src: '../jquery.extend/jquery.extend.js',
                dest: 'lib/jquery.extend.js'
            }
        }
    });

    // These plugins provide necessary tasks.
    require('load-grunt-tasks')(grunt, {
        scope: 'devDependencies'
    });

    // Default task(s).
    grunt.registerTask('default', ['copy']);
}