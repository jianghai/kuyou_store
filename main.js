/**
 * Global module
 */
(function() {

    // require.js
    require.config({
        baseUrl: 'modules'
    });

    vjs.leftTag = '<#';
    vjs.rightTag = '#>';

    window.App = {

        loading: $('#template-loading').html(),

        error: $('#template-error').html(),

        noData: vjs($('#template-noData').html()),

        dataURL: {
            profile_shop: 'LoadShopAction',
            profile_list: 'SearchUserinfoAction',
            profile_detail: 'GetUserinfoAction',
            profile_detail_dealRecord: 'GetUserOrdersAction'
        },

        dataURL: {
            profile_shop: 'data/shop.json',
            profile_list: 'data/profile.json',
            profile_detail: 'data/detail.json',
            profile_detail_dealRecord: 'data/dealRecord.json'
        },

        // templateType: 'jsp',

        templateType: 'html'
    };

    // global view
    var AppView = Trunk.View.extend({

        el: $('body'),

        events: {
            'click .logo': 'onNavi',
            'click .navi a': 'onNavi',
            'click .breadcrumbs a': 'onNavi'
        },

        onNavi: function(e) {
            var href = e.target.href;
            if (href === location.href) {
                $(window).trigger('hashchange');
            }
        },

        init: function() {

            // load svg document
            var iconsURL = 'icons.svg';
            var iconsContainer = $('<div>');
            iconsContainer.css('display', 'none');
            this.el.append(iconsContainer);
            $.ajax({
                url: iconsURL,
                dataType: 'text'
            }).done(function(res) {
                iconsContainer.html(res);
            });

            this.content = this.$('.content');


            var Breadcrumbs = Trunk.Model.extend({

                hashMap: {
                    profile: {
                        _name: '顾客档案查询',
                        detail: {
                            _name: '档案详情页'
                        }
                    },
                    permission: {
                        _name: '用户权限管理',
                        add: {
                            _name: '创建用户'
                        },
                        edit: {
                            _name: '修改用户'
                        }
                    }
                },

                getNodes: function() {
                    var _nodes = [];
                    var _map = this.hashMap;
                    var _url = [];
                    var _hash = location.hash;
                    var _key0;
                    if (!_hash) {
                        _key0 = Object.keys(_map)[0];
                        _nodes = [{
                            url: _key0,
                            name: _map[_key0]._name
                        }];
                    } else {
                        _hash = _hash.slice(1).split('?')[0];
                        $.each(_hash.split('/'), function(i, v) {
                            _map = _map[v];
                            _url.push(v);
                            _nodes.push({
                                url: _url.join('/'),
                                name: _map._name
                            });
                        });
                    }
                    return _nodes;
                }
            });

            var BreadcrumbsView = Trunk.View.extend({

                model: Breadcrumbs,

                el: this.$('.breadcrumbs'),

                template: '.template-breadcrumbs'
            });

            var SideMenu = Trunk.Model.extend({

                url: '',
                
                fetch: function() {
                    // form the server
                    this.set({
                        menu: [{
                            key: 'profile',
                            name: '顾客档案查询',
                            icon: 'profile'
                        }]
                    });
                }
            });

            var SideMenuView = Trunk.View.extend({

                model: SideMenu,

                el: '.side-menu',

                template: '.template-side-menu',

                highlight: function(hash) {
                    hash = hash.split('/');
                    this.el.find('.active').removeClass('active');
                    this.el.find('[data-node="' + hash[0] + '"').addClass('active');
                }
            });

            this.breadcrumbs = new BreadcrumbsView();

            this.childViews = {

                sideMenu: new SideMenuView()
            };
        }
    });

    var Router = Trunk.Router.extend({

        router: {
            'permission/edit': 'user',
            'permission/add': 'user',
            '*': 'show'
        },

        user: function() {
            this.show('permission/user');
        },

        show: function(hash) {

            hash = hash || 'profile';

            // breadcrumbs update
            app.breadcrumbs.model.set({
                nodes: app.breadcrumbs.model.getNodes()
            });

            // menu highlight
            app.childViews.sideMenu.highlight(hash);

            hash = hash.split('/').join('.');

            require([hash], function(view) {
                app.content.html(view.el);
                if (!view.html) {
                    $.get('html/' + hash + '.' + App.templateType, function(str) {
                        view.html = str;
                        view.show();
                    });
                } else {
                    view.show();
                }
            });
        }
    });

    var app = new AppView();

    app.show();

    new Router();

})(window);