define([
    'components/base',
    'components/select',
    'components/search',
    'components/grid',
    'components/dialog'
], function(base, Select, Search, Grid, dialog) {

    var View = Trunk.View.extend({

        tag: 'div',

        className: 'profile',

        events: {
            'click .detail': 'onDetail'
        },

        onDetail: function(e) {
            var mobile = $(e.target).attr('data-mobile');
            dialog.load(this.detail.el);

            this.detail.model.resetParam({
                mobile: mobile
            });
        },

        init: function() {

            var LinkselectView = Trunk.View.extend({

                el: '.linkselect',

                render: function() {

                    if (!this.el.length) return;

                    this.el.empty();

                    this.el.append(this.province.el, this.city.el, this.shop.el);

                    if (!this.data) {
                        this.province.model.fetch();
                    } else {
                        this.province.render();
                    }
                    this.city.model.reset();
                    this.shop.model.reset();
                },

                init: function() {

                    var _this = this;

                    this.selected = {};

                    this.province = new Select({
                        className: 'select select-sm',
                        modelProperty: {
                            defaults: {
                                title: '所属省份'
                            },
                            url: App.dataURL.profile_shop,
                            onFetch: function(res) {
                                _this.data = res;
                                this.set({
                                    items: $.map(Object.keys(res), function(v) {
                                        return {
                                            key: v,
                                            value: v
                                        };
                                    })
                                });
                            }
                        }
                    });

                    this.city = new Select({
                        className: 'select select-sm',
                        modelProperty: {
                            defaults: {
                                title: '所属市'
                            }
                        }
                    });

                    this.shop = new Select({
                        className: 'select select-sm',
                        modelProperty: {
                            defaults: {
                                title: '所属门店'
                            }
                        }
                    });


                    this.province.on('change', function(province) {

                        _this.selected = {
                            province: province
                        };
                        _this.trigger('change', _this.selected);

                        _this._province = province;
                        _this.city.model.set({
                            items: province && $.map(Object.keys(_this.data[province]), function(v) {
                                return {
                                    key: v,
                                    value: v
                                };
                            }) || []
                        });

                        _this.shop.model.reset();
                    });

                    this.city.on('change', function(city) {

                        _this.selected.city = city;
                        delete _this.selected.shop;
                        _this.trigger('change', _this.selected);

                        if (!city) {
                            _this.shop.model.reset();
                            return;
                        }

                        _this.shop.model.set({
                            items: $.map(_this.data[_this._province][city], function(v) {
                                return $.map(v, function(v, k) {
                                    return {
                                        key: k,
                                        value: v
                                    };
                                });
                            })
                        });
                    });

                    this.shop.on('change', function(shop) {
                        _this.selected.shop = shop;
                        _this.trigger('change', _this.selected);
                    });
                }
            });

            var linkselect = new LinkselectView();
            var search = new Search({
                modelProperty: {
                    defaults: {
                        placeholder: '在此输入手机号'
                    }
                },
                show: function() {
                    this.model.reset();
                }
            });
            var grid = new Grid({
                modelProperty: {
                    url: App.dataURL.profile_list,
                    defaultParam: {
                        limit: 12
                    }
                },
                el: '.grid',
                template: '.template-grid',
                show: function() {
                    this.model.resetParam();
                }
            });

            linkselect.on('change', function(attr) {
                grid.model.resetParam({
                    province: attr.province,
                    city: attr.city,
                    shop_id: attr.shop
                });
                search.model.reset();
            });
            search.on('search', function(keyword) {
                grid.model.resetParam({
                    mobile: keyword
                });
                linkselect.province.el.prop('selectedIndex', 0);
                linkselect.city.model.reset();
                linkselect.shop.model.reset();
            });

            this.childViews = {
                linkselect: linkselect,
                search: search,
                grid: grid
            };

            var Detail = base.Model.extend({

                url: App.dataURL.profile_detail
            });

            var DetailView = base.View.extend({

                model: Detail,

                tag: 'div',

                className: 'profile-detail',

                template: '#template-profile-detail',

                init: function() {

                    var _parent = this;

                    this.childViews = {
                        dealRecord: new Grid({
                            modelProperty: {
                                url: App.dataURL.profile_detail_dealRecord,
                                defaultParam: {
                                    limit: 5
                                }
                            },
                            el: '.deal-record',
                            template: '#template-profile-detail-dealRecord',
                            show: function() {
                                this.model.resetParam({
                                    mobile: _parent.model.param.mobile
                                });
                            }
                        })
                    };
                }
            });

            this.detail = new DetailView();
        }
    });

    var view = new View();

    return view;
});