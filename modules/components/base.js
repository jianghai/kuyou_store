define(function() {

    var Model = Trunk.Model.extend({

        paramStringify: function() {
            return $.jsonParam(this.param);
        },

        resetParam: function(param) {
            this.param = $.extend({}, this.defaultParam, param);
            this.fetch();
        },

        setParam: function(param) {
            if (!this.param) {
                this.param = this.defaultParam || {};
            }
            $.extend(this.param, param);
            this.fetch();
        },

        onFetch: function(res) {
            this.set(res);
        },

        fetch: function() {
            var _this = this;
            this.trigger('sync');
            $.ajax({
                url: this.url,
                data: this.paramStringify(),
                dataType: 'json'
            }).done(function(res) {
                if (!res) {
                    return _this.trigger('noData');
                }
                _this.onFetch(res);
            }).fail(function() {
                _this.trigger('error');
            });
        }
    });

    var View = Trunk.View.extend({

        loading: function() {
            this.el.html(App.loading);
        },

        error: function() {
            this.el.html(App.error);
        },

        noData: function() {
            this.el.html(App.noData());
        },

        init: function() {
            this.listen(this.model, 'sync', this.loading);
            this.listen(this.model, 'error', this.error);
            this.listen(this.model, 'noData', this.noData);
        }
    });

    return {
        Model: Model,
        View: View
    };
});