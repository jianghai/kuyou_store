define(function() {

    var View = Trunk.View.extend({

        tag: 'div',

        className: 'dialog-layer',

        // template: '#template-dialog',

        events: {
            'wheel .dialog-body': 'onWheel',
            'click': 'onClick',
            'click .dialog-close': 'close'
        },

        onWheel: function(e) {
            e.currentTarget.scrollTop -= e.originalEvent.wheelDeltaY;
            return false;
        },

        onClick: function(e) {
            if ($(e.target).hasClass(this.className)) {
                this.close();
            }
        },

        close: function() {
            this.el.removeClass('open');
        },

        open: function() {
            this.el.addClass('open');
            // this.setPosition();
        },

        // setPosition: function() {
        //     var height = this.content.height();
        //     var width = this.content.width();
        //     this.content.css({
        //         marginTop: -height / 2 + 'px',
        //         marginLeft: -width / 2 + 'px'
        //     });
        // },

        load: function(el) {
            this.el.html(el);
            this.open();
        },

        init: function() {
            $('body').append(this.el);
        }
    });

    return new View();
});