define([
    'components/base',
    'components/pagination'
], function(base, Pagination) {

    var Model = base.Model.extend({

        init: function() {
            this.defaultParam = {
                limit: 10
            };
        }
    });

    var View = base.View.extend({

        model: Model,

        pageTo: function(page) {
            this.model.setParam({
                curpage: page
            });
        },

        afterRender: function() {
            this.pagination.model.parse({
                curpage: this.model.attr.curpage,
                totalpage: this.model.attr.totalpage
            });
            this.el.append(this.pagination.el);
        },

        init: function() {

            this.pagination = new Pagination();

            this.listen(this.pagination, 'change', this.pageTo);
        }
    });

    return View;
});