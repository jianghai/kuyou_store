define(function() {

    var Model = Trunk.Model.extend({

        fetch: function() {
            var _this = this;
            $.ajax({
                url: this.url
            }).done(function(res) {
                _this.onFetch(res);
            });
        }
    });

    var View = Trunk.View.extend({

        model: Model,

        tag: 'select',

        className: 'select',

        template: '#template-select',

        events: {
            'change': 'onChange'
        },

        onChange: function(e) {
            var target = $(e.target);
            this.trigger('change', target.val());
        }
    });

    return View;
});