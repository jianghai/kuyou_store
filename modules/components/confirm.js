define(['components/dialog'], function(dialog) {

    var Model = Trunk.Model.extend({

    });

    var View = Trunk.View.extend({

        model: Model,

        tag: 'div',

        className: 'confirm',

        template: '#template-confirm',

        events: {
            'click .confirm': 'onConfirm'
        },

        beforeRender: function() {
            dialog.load(this.el);
        },

        onConfirm: function() {
            this.afterConfirm && this.afterConfirm();
            dialog.close();
        },

        confirm: function(attr, afterConfirm) {
            this.model.set(attr);
            this.afterConfirm = afterConfirm;
        }
    });

    return new View();
});