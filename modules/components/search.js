define(function () {

    var Model = Trunk.Model.extend({

    });
    
    var View = Trunk.View.extend({

        model: Model,

        el: '.search',

        template: '#template-search',

        events: {
            'keyup .input-text': 'onKeyup',
            'submit': 'onSubmit'
        },

        onKeyup: function(e) {
            if (this.searched) {
                var value = e.target.value;
                if (value === '') {
                    this.model.attr.value = value;
                    this.trigger('search', value);
                    this.searched = false;
                }
            }
        },

        onSubmit: function(e) {
            e.preventDefault();
            this.searched = true;
            var form = $(e.target);
            var input = $(form[0].keyword);
            var kw = input.val();
            if (!kw) return input.focus();

            this.model.attr.value = kw;

            this.trigger('search', $.trim(kw));
        }
    });

    return View;
});