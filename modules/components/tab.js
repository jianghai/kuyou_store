define(function() {

    var View = Trunk.View.extend({

        events: {
            'click .tab-item': 'onTab'
        },

        onTab: function(e) {
            var target = $(e.target);
            target.addClass('active').siblings().removeClass('active');
            this.trigger('change', target.attr('data-key'));
        }
    });

    return View;
});