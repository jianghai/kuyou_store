// This is a task module

define(function() {

    $('body').on('click', function() {

        $('.dropdown').removeClass('open');
    
    }).on('click', '.drop-toggle', function(e) {
        
        var target = $(e.target);
        
        var dropdown = $(e.currentTarget).parent();

        var closestDropdown = dropdown.closest('.dropdown');

        (closestDropdown.length && closestDropdown || $(document))
            .find('.dropdown').not(dropdown).removeClass('open');

        dropdown.toggleClass('open');

        // position reset
        if (dropdown.hasClass('open')) {
            var o_t = target.offset().top;
            var layer = target.next();
            var _h = layer.height();
            if ($('body').height() - o_t - target.height() < _h && o_t > _h) {
                layer.css('top', '-' + (_h - 2) + 'px');
            }
        }

        return false;

    }).on('click', '.drop-layer', function(e) {

        e.stopPropagation();
        
        var target = $(e.currentTarget);
        
        target.find('.dropdown').removeClass('open');
    });
});