define([
    'components/confirm'
], function(confirm) {

    var View = Trunk.View.extend({

        tag: 'div',

        className: 'permission',

        events: {
            // 'click': 'aa'
        },

        aa: function() {
            confirm.confirm({
                title: '您是否要禁用该用户试用门店管理系统？'
            }, function() {
                console.log(1);
            });
        },

        init: function() {

            var SearchView = Trunk.View.extend({

                el: '.form-inline',

                events: {
                    'submit': 'onSubmit'
                },

                onSubmit: function() {

                }
            })
        }
    });

    return new View();
});