define([
    'components/base', 
    'components/grid'
], function(base, Grid) {

    var View = Trunk.View.extend({

        tag: 'div',

        className: 'profile-detail',

        afterRender: function() {
            var record = this.childViews.detail.childViews.dealRecord;
            this.childViews.detail.model.param = record.model.param = Trunk.get;
            if (!record.template) {
                record.template = vjs(this.$('.template-dealRecord').html());
            }
        },

        init: function() {

            var Detail = base.Model.extend({

                url: App.dataURL.profile_detail
            });

            var DetailView = base.View.extend({

                model: Detail,

                el: '.detail',

                template: '.template-detail',

                init: function() {
                    var dealRecord = new Grid({
                        modelProperty: {
                            url: App.dataURL.profile_detail_dealRecord,
                            param: Trunk.get
                        },

                        el: '.deal-record'
                    });

                    this.childViews = {
                        dealRecord: dealRecord
                    };
                }
            });

            this.childViews = {
                detail: new DetailView()
            };
        }
    });

    return new View();
});