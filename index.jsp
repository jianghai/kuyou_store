<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>酷友门店营销系统</title>
    <link rel="stylesheet" href="kuyou.css">
    <link rel="stylesheet" href="main.css">
</head>
<body class="body">
    <header class="header flex">
        <a href="#" class="logo">
            <svg class="icon">
                <use xlink:href="#logo"></use>
            </svg>
            酷友门店营销系统
        </a>
        <div class="user flex-1">
            <svg class="icon">
                <use xlink:href="#user"></use>
            </svg>
            欢迎您，
            <span class="name"><%=(String)session.getAttribute("username") %></span>
            <a href="LogoutAction">
                <svg class="icon">
                    <use xlink:href="#logout"></use>
                </svg>
                退出
            </a>
        </div>
    </header>
    <section class="wrapper flex">
        <section class="sidebar">
            <ul class="navi side-menu">
                <script type="text" class="template-side-menu">
                <# $.each(data.menu, function() { #>
                <li data-node="<#- this.key #>">
                    <a href="#<#- this.key #>">
                        <svg class="icon">
                            <use xlink:href="#<#- this.icon #>"></use>
                        </svg>
                        <#- this.name #>
                    </a>
                </li>
                <# }); #>
                </script>
<!--                 <li class="active">
                    <a href="#profile">
                        <svg class="icon">
                            <use xlink:href="#profile"></use>
                        </svg>
                        顾客档案查询
                    </a>
                </li>
                <li>
                    <a href="#permission">
                        <svg class="icon">
                            <use xlink:href="#gear"></use>
                        </svg>
                        用户权限管理
                    </a>
                </li> -->
            </ul>
        </section>
        <section class="main">
            <div class="breadcrumbs">
                <script type="text" class="template-breadcrumbs">
                <svg class="icon">
                    <use xlink:href="#home"></use>
                </svg>
                <a href="#">首页</a>&nbsp;>&nbsp;
                <# $.each(data.nodes, function(i, val) {
                    if (i != data.nodes.length - 1) { #>
                        <a href="#<#- this.url #>"><#- this.name #></a>&nbsp;>&nbsp;
                    <# } else { #>
                        <span><#- this.name #></span>
                    <# }
                }); #>
                </script>
            </div>
            <div class="content"></div>
        </section>
    </section>
    <footer class="footer">
        TCL集团股份有限公司版权所有 粤ICP备05040863号 &copy;2010-2011 TCL CORPORATION All Rights Reserved.
    </footer>

    <div class="screen-toggle">
        <span class="arrow"></span>
    </div>

    <script type="text" id="template-loading">
    <div class="request-tip loading">
        <span class="tip">正在加载</span>
    </div>
    </script>
    <script type="text" id="template-error">
    <div class="request-tip error">
        <span class="tip">数据加载错误，请重试。</span>
    </div>
    </script>
    <script type="text" id="template-noData">
    <div class="request-tip noData">
        <span class="tip"><#- (data||'暂无数据') #></span>
    </div>
    </script>
    
    <script type="text" id="template-dialog">
    <div class="dialog-content"></div>
    </script>

    <script type="text" id="template-select">
    <option value=""><#- data.title #></option>
    <# data.items && $.each(data.items, function() { #>
    <option value="<#- this.key #>" <#- (data.selected===this.key?'selected':'') #>><#- this.value #></option>
    <# }); #>
    </script>

    <script type="text" id="template-confirm">
    <h1><#- data.title #></h1>
    <div class="btns">
        <button class="btn btn-sm btn-primary confirm">确定</button>
        <button class="btn btn-sm btn-default dialog-close">取消</button>
    </div>
    </script>
    
    <script type="text" id="template-search">
    <div class="input-text-group inline">
        <svg class="icon">
            <use xlink:href="#search"></use>
        </svg>
        <input type="text" autocomplete="off" name="keyword" class="input-text input-sm" placeholder="<#- data.placeholder #>" value="<#- (data.value||'') #>">
    </div>
    <button type="submit" class="btn btn-sm btn-primary">搜索</button>
    </script>

    <script type="text" id="template-pagination">
    <ul class="pagination">
        <# if (data.current > 1) { #>
        <li><a href="#" data="prev" class="page">&lt;</a></li>
        <# } #>
        <# if (data.start !== 1) { #>
            <# for (var i = 1; i <= 2; i++) { #>
            <li><a href="#" data="<#- i #>" class="page"><#- i #></a></li>
            <# } #>
            <li><div class="not-page">...</div></li>
        <# } #>
        <# for (var i = data.start; i <= data.end; i++) { #>
        <li <#- (data.current===i?'class="active"':'') #>><a href="#" data="<#- i #>" class="page"><#- i #></a></li>
        <# } #>
        <# if (data.end !== data.counts) { #>
            <li><div class="not-page">...</div></li>
        <# } #>
        <# if (data.current < data.counts) { #>
        <li><a href="#" data="next" class="page">&gt;</a></li>
        <# } #>
        <li>
            <div class="not-page">共 <#- data.counts #> 页</div>
        </li>
        <li>
            <form class="form-paging">
                <label>跳转到</label><input type="text" autocomplete="off" name="page" class="input-text input-sm">
                <button type="submit" class="btn btn-sm">确定</button>
            </form>
        </li>
    </ul>
    </script>

    <script src="lib/jquery.min.js"></script>
    <script src="lib/vjs.min.js"></script>
    <script src="lib/trunk.min.js"></script>
    <script src="lib/jquery.extend.js"></script>
    <script src="lib/require.min.js"></script>
    <script src="main.js"></script>
</body>
</html>