<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="head flex">
    <form action="" class="form-inline flex-1">
        <label>
            用户名
            <input type="text" class="input-text input-sm" placeholder="请输入ID/用户名">
        </label>
        <label>
            联系人
            <input type="text" class="input-text input-sm" placeholder="请输入联系人">
        </label>
        <label>
            状态
            <select class="select select-sm">
                <option value="">全部</option>
                <option value="">启用</option>
                <option value="">禁用</option>
            </select>
        </label>
        <button type="submit" class="btn btn-sm btn-primary">搜索</button>
    </form>
    <div class="operate">
        <a href="#permission/add" class="btn btn-primary btn-sm">创建系统用户</a>
    </div>
</div>
<div class="module-group">
    <div class="module-head flex">
        <div class="module-title">
            <span>
                <svg class="icon">
                    <use xlink:href="#file"></use>
                </svg>
                系统用户列表
            </span>
        </div>
    </div>
    <div class="module-body">
        <div class="module grid">
            <table class="table">
                <thead>
                    <tr>
                        <td>手机</td>
                        <td>姓名</td>
                        <td>地区</td>
                        <td>最近购买时间</td>
                        <td>最近营销时间</td>
                        <td>操作</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1381000000</td>
                        <td>张喆</td>
                        <td>北京</td>
                        <td>2014.11.11</td>
                        <td>2014.11.10</td>
                        <td>
                            <a href="#permission/edit" class="btn btn-sm">编辑</a>
                            <a href="" class="btn btn-sm remove">删除</a>
                            <a href="" class="btn btn-sm disable">禁用</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>