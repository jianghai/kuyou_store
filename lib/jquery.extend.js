/**
 * Complementary features for jQuery
 * See: http://github.com/jianghai for details.
 */

(function($) {

    'use strict';

    $.extend(true, $, {

        /**
         * Data format
         */
        format: {

            // 3400011.1 => '3,400,011.1'
            number: function(number, fix) {
                if (!number) return 0;
                return this.toFixed(number, fix).toString().replace(/(\d{1,3})(?=(\d{3})+(?:$|\.))/g, '$1,');
            },

            // 1.15 => 1.2
            toFixed: function(number, precision) {
                var multiplier = Math.pow(10, precision || 0);
                return Math.round(number * multiplier) / multiplier;
            },

            // 0.234 => '23.4%'
            percent: function(number, fix) {
                return this.toFixed(number * 100, fix) + '%';
            },

            // yyyy-mm-dd hh:ii:ss '2014-09-24 18:15:09'
            date: function(date, format) {

                if (!date) return;

                var d = new Date(date);

                var rules = {
                    'y+': d.getFullYear(),
                    'm+': d.getMonth() + 1,
                    'd+': d.getDate(),
                    'h+': d.getHours(),
                    'i+': d.getMinutes(),
                    's+': d.getSeconds()
                };

                $.each(rules, function(i, v) {
                    format = format.replace(new RegExp(i), function(str) {
                        var len = str.length;
                        var res = v.toString();
                        if (str[0] === 'y') {
                            res = res.slice(-len);
                        }
                        if (res.length < len) {
                            res = '0' + res;
                        }
                        return res;
                    });
                });

                return format;
            }
        },


        /**
         * Replace html chracter with entities
         */
        htmlEscape: function(str) {
            return str.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/&/g, '&amp;');
        },

        /**
         * Different from $.param is that the value has been stringified before serialization
         */
        jsonParam: function(obj) {
            if (!obj) return;
            var param = [];
            $.each(obj, function(k, item) {
                if (!item) return;
                param.push(k + '=' + (typeof item === 'object' ? JSON.stringify(item) : item));
            });
            return param.join('&');
        }
    });


})(jQuery);